#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>

typedef struct {
	char name[20];
	int length;
	int aver_depth;
} RIVER;

int main()
{
	setlocale(LC_ALL, "rus");
	FILE *fp;
	RIVER mass[12] = { 0 };
	fp = (FILE*)fopen("file.txt", "r");
	int sum = 0, key = 0;
	fseek(fp, 0, SEEK_SET);
	for (int i = 0; i < 12; i++)
	{
		fscanf(fp, "%s", mass[i].name);
		fscanf(fp, " %ld", &mass[i].length);
		fscanf(fp, " %d;", &mass[i].aver_depth);
		if (mass[i].aver_depth < 50)
		{
			sum += mass[i].length;
		}
	}
	while (1)
	{
		printf("\n����������� ���������:\n1 - �������� �������\n2 - ���������� ����� ����� ��� � �������� ������ 50 �\n3 - ���������� ������ ���������\n������� �������: ");
		scanf(" %d", &key);
		switch (key)
		{
		case 1: printf("\n|    ��������    | ������, �� | ��.�������, � |\n");
			printf("|----------------+------------+---------------|\n");
			for (int i = 0; i < 12; i++)
			{
				printf("|%-16s|", mass[i].name);
				printf(" %-11d|", mass[i].length);
				printf(" %-14d|\n", mass[i].aver_depth);
			}
			break;
		case 2: printf("����� ����� ���, ������� ������� ������ 50� = %d�\n", sum); 
			break;
		case 3: return 0;
		default: printf("�� ����� �������������� �������");
			break;
		}
	}
	fclose(fp);
	return 0;
}